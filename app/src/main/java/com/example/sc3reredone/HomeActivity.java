package com.example.sc3reredone;

import android.app.Dialog;
import android.app.Person;
import android.content.res.Configuration;
import android.graphics.Color;
import android.support.annotation.NonNull;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import java.util.ArrayList;

public class HomeActivity extends AppCompatActivity {

    private FloatingActionButton floatButton;
    private ArrayList<PersonnelData> personnelList = new ArrayList<>();
    private PersonnelAdapter pAdapter;
    private RecyclerView personnel_rv;
    private RecyclerView.LayoutManager lManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        personnel_rv = findViewById(R.id.listPersonnel);

        floatButton = findViewById(R.id.floatButton);
        floatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showPersonnelDialog();
            }
        });


        personnel_rv = findViewById(R.id.listPersonnel);
        lManager = new LinearLayoutManager(this);
        pAdapter = new PersonnelAdapter(this, personnelList);

        personnel_rv.setAdapter(pAdapter);
        personnel_rv.setLayoutManager(lManager);

        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(personnel_rv);


    }

    private void showPersonnelDialog() {
        final Dialog dialog = new Dialog(this);
        dialog.setContentView(R.layout.personnel_dialog);
        final TextView editNama = dialog.findViewById(R.id.enterNama);
        final TextView editPekerjaan = dialog.findViewById(R.id.enterPekerjaan);
        Button submitButton = dialog.findViewById(R.id.submitBtn);
        Button cancelButton = dialog.findViewById(R.id.cancelBtn);
        final Spinner spinnerGender = dialog.findViewById(R.id.genderSpinner);

        populateSpinner(spinnerGender);

        cancelButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String nama = editNama.getText().toString();
                String pekerjaan = editPekerjaan.getText().toString();
                String gender = spinnerGender.getSelectedItem().toString();

                PersonnelData personnel = new PersonnelData(nama, pekerjaan, gender);

                personnelList.add(personnel);
                dialog.dismiss();


            }
        });

        dialog.show();
    }


    private void populateSpinner(Spinner spinner) {
        ArrayList<String> gender = new ArrayList<>();
        gender.add("Female");
        gender.add("Male");
        spinner.setAdapter(new ArrayAdapter<String>(HomeActivity.this,
                android.R.layout.simple_expandable_list_item_1, gender));
    }

    ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {
        @Override
        public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
            return false;
        }

        @Override
        public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
            pAdapter.removeItem(viewHolder.getAdapterPosition());

        }

    };

}







    /*private void  layoutManagerPortrait(){
        LinearLayoutManager llManager = new LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);
        personnel_rv.setLayoutManager(llManager);
    }

    private void layoutManagerLandscape(){
        GridLayoutManager llManager = new GridLayoutManager(this, 2, GridLayoutManager.VERTICAL, false);
        personnel_rv.setLayoutManager(llManager);
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if(newConfig.orientation == Configuration.ORIENTATION_LANDSCAPE){
            layoutManagerLandscape();
        } else {
            layoutManagerPortrait();
        }
    }*/

