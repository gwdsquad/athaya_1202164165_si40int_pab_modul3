package com.example.sc3reredone;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

public class PersonnelAdapter extends RecyclerView.Adapter<PersonnelAdapter.ViewHolder>{

    //public static final String EXTRA_MESSAGE = "com.example.android.sc3reredone.extra.MESSAGE";



    private Context context;
    private ArrayList<PersonnelData> personnels;

    public class ViewHolder extends RecyclerView.ViewHolder {
        TextView nama;
        TextView pekerjaan;
        ImageView img;
        LinearLayout card;

        ViewHolder(View v){
            super (v);
            nama = v.findViewById(R.id.personnelNama);
            pekerjaan = v.findViewById(R.id.personnelPekerjaan);
            img = v.findViewById(R.id.personnelImg);
            card = v.findViewById(R.id.personnelCard);
        }
    }

    public PersonnelAdapter(Context context, ArrayList<PersonnelData> personnels) {
        this.context = context;
        this.personnels = personnels;
    }

    @NonNull
    @Override
    public PersonnelAdapter.ViewHolder onCreateViewHolder
            (@NonNull ViewGroup parent, int i) {
        View v = (View) LayoutInflater.from(parent.getContext())
                .inflate(R.layout.personnel_card, parent, false);

        return new ViewHolder(v);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {



        final PersonnelData personnel = personnels.get(position);
        holder.nama.setText(personnel.getNama());
        holder.pekerjaan.setText(personnel.getPekerjaan());

       final String pNama = personnel.getNama();

        holder.card.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent = new Intent(context, PersonnelDetail.class);

                //intent.putExtra("nama", pNama);


                context.startActivity(intent);


            }
        });

        if (personnel.getGender().equals("Female")){
            holder.img.setBackgroundResource(R.drawable.femaleicon);
        } else {
            holder.img.setBackgroundResource(R.drawable.maleicon);
        }
    }

    public void removeItem(int position){
        personnels.remove(position);

        notifyItemRemoved(position);
    }



    @Override
    public int getItemCount() {
        return personnels.size();
    }
}
