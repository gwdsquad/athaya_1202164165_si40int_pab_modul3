package com.example.sc3reredone;

public class PersonnelData {

    public String nama;
    public String pekerjaan;
    public String gender;

    public PersonnelData(String nama, String pekerjaan, String gender) {
        this.nama = nama;
        this.pekerjaan = pekerjaan;
        this.gender = gender;
    }

    public String getNama() {
        return nama;
    }

    public String getPekerjaan() {
        return pekerjaan;
    }

    public String getGender() {
        return gender;
    }
}
